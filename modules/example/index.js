import {ApplicationService} from "@themost/common/app";
import {DataConfigurationStrategy, ODataModelBuilder} from "@themost/data";
import path from 'path';
import fs from 'fs';
import pluralize from 'pluralize';

class ExamplePlugin extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        /**
         * get configuration strategy
         * @type {DataConfigurationStrategy}
         */
        const configuration = app.getConfiguration().getStrategy(DataConfigurationStrategy);
        // noinspection JSValidateTypes
        /**
         * get model builder
         * @type {ODataModelBuilder}
         */
        const builder = app.getStrategy(ODataModelBuilder);
        // load models
        const files = fs.readdirSync(path.resolve(__dirname, 'config/models'));
        files.forEach( file => {
            // load model definition
            const model = require(path.resolve(__dirname, 'config/models', file));
            // try to get model from application (if model definition does not exists in parent configuration)
            if (configuration.model(model.name) == null) {
                // set model definition
                configuration.setModelDefinition(model);
                if (builder) {
                    // add entity set (for odata endpoints)
                    builder.addEntitySet(model.name, pluralize.plural(model.name));
                    // check if model is hidden
                    if (model.hidden) {
                        // and ignore it
                        builder.ignore(model.name);
                    }
                }
            }
        });
    }
}


module.exports.ExamplePlugin = ExamplePlugin;
