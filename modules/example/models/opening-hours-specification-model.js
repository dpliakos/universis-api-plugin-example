import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
let StructuredValue = require('./structured-value-model');
/**
 * @class
 
 * @property {Date} validFrom
 * @property {Date} validThrough
 * @property {Duration} opens
 * @property {Duration} closes
 * @property {number} dayOfWeek
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('OpeningHoursSpecification')
class OpeningHoursSpecification extends StructuredValue {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = OpeningHoursSpecification;