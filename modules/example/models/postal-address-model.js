import {EdmMapping} from '@themost/data/odata';
let ContactPoint = require('./contact-point-model');
/**
 * @class
 
 * @property {string} postOfficeBoxNumber
 * @property {string} streetAddress
 * @property {string} addressCountry
 * @property {string} addressRegion
 * @property {string} postalCode
 * @property {string} addressLocality
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('PostalAddress')
class PostalAddress extends ContactPoint {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = PostalAddress;
