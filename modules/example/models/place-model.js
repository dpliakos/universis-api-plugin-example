import {EdmMapping} from '@themost/data/odata';
let Thing = require('./thing-model');
/**
 * @class
 
 * @property {OpeningHoursSpecification|any} openingHoursSpecification
 * @property {string} globalLocationNumber
 * @property {number} maximumAttendeeCapacity
 * @property {string} map
 * @property {string} branchCode
 * @property {string} hasMap
 * @property {PostalAddress|any} address
 * @property {string} logo
 * @property {string} telephone
 * @property {GeoCoordinates|any} geo
 * @property {Place} containedInPlace
 * @property {boolean} publicAccess
 * @property {string} maps
 * @property {string} faxNumber
 * @property {boolean} isAccessibleForFree
 * @augments {DataObject}
 */
@EdmMapping.entityType('Place')
class Place extends Thing {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Place;
